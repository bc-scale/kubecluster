


data "template_file" "network_config-worker1" {
  template = "${file("${path.module}/configs/network_config-worker1.cfg")}"
}


resource "libvirt_cloudinit_disk" "commoninit-worker1" {
  name = "commoninit-worker1.iso"
  user_data = data.template_file.user_data.rendered
  network_config = data.template_file.network_config-worker1.rendered
  pool = var.kvm_pool
}


resource "libvirt_volume" "worker1" {
  name           = "worker1.qcow2"
  base_volume_id = libvirt_volume.base-image-resized.id
  pool = var.kvm_pool
}

resource "libvirt_volume" "worker1-data1" {
  name           = "worker1-data1.qcow2"
  base_volume_id = libvirt_volume.base-data-disk.id
  pool = var.kvm_pool
}

resource "libvirt_volume" "worker1-data2" {
  name           = "worker1-data2.qcow2"
  base_volume_id = libvirt_volume.base-data-disk.id
  pool = var.kvm_pool
}


resource "libvirt_domain" "worker1" {
  name = "worker1"
  memory = "8096"
  vcpu = 3

  cloudinit = libvirt_cloudinit_disk.commoninit-worker1.id

  network_interface {
    bridge = var.kvm_bridge_interface
  }

  # IMPORTANT
  # Ubuntu can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
      type        = "pty"
      target_type = "virtio"
      target_port = "1"
  }

  disk {
       volume_id = libvirt_volume.worker1.id
  }

  disk {
       volume_id = libvirt_volume.worker1-data1.id
  }

  disk {
       volume_id = libvirt_volume.worker1-data2.id
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}
