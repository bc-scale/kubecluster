infra-lab = "labs"
infra-lab-network = "192.168.100.0/24" # For isolated_mode - Review configuration file sfor each node
infra-os-base-url = "https://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.img"
infra-node-prefix="lab"
private_key_path = "../keys/provision"
kvm_pool = "labs"
